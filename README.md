# Taller 2

## Requerimientos

- Desarrollar una Chrome App tomando como base el Taller 1, agregando:

1. Un formulario para configurar una simulación personalizada en una nueva opción llamada "Planeta genérico".
2. El formulario debe abrir en un modal.
3. Mejorar los estilos utilizando Bootstrap o Materialize.
4. Implementar jQuery para manejar los eventos del ratón.



## Ejemplo de referencia

> https://phet.colorado.edu/sims/html/gravity-and-orbits/latest/gravity-and-orbits_all.html?locale=es
